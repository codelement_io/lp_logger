var fs = require('fs'),
    path = require('path'),
    util = require('util'),
    winston = require('winston');


winston.transports.DailyRotateFile = require('winston-daily-rotate-file');

//
// ### function HourlyRotateFile (options)
// #### @options {Object} Options for this instance.
// Constructor function for the HourlyRotateFile transport object responsible
// for persisting log messages and metadata to one or more files.
//
var HourlyRotateFile = exports.HourlyRotateFile = function (options) {
    winston.transports.DailyRotateFile.call(this, options);
    this.tailable = options.tailable || false;
    this.defaultMetaData = options.meta || {};
};

//
// Inherit from `winston.transports.DailyRotateFile`.
//
util.inherits(HourlyRotateFile, winston.transports.DailyRotateFile);

// Overload log function to add default metadata
HourlyRotateFile.prototype.log = function (level, msg, metaData, callback) {
    var meta = Object.assign({}, metaData || {}, this.defaultMetaData);
    HourlyRotateFile.super_.prototype.log.apply(this, [level, msg, meta, callback]);
};

//
// Expose the name of this Transport on the prototype
//
HourlyRotateFile.prototype.name = 'hourlyRotateFile';

//
// Define a getter so that `winston.transports.HourlyRotateFile`
// is available and thus backwards compatible.
//
winston.transports.HourlyRotateFile = HourlyRotateFile;

//
// ### @private function _createStream ()
// Attempts to open the next appropriate file for this instance
// based on the common state (such as `maxsize` and `_basename`).
//
HourlyRotateFile.prototype._createStream = function () {
    var self = this;
    this.opening = true;

    (function checkFile (target) {
        var fullname = path.join(self.dirname, target);

        //
        // Creates the `WriteStream` and then flushes any
        // buffered messages.
        //
        function createAndFlush (size) {
            if (self._stream) {
                self._stream.end();
                self._stream.destroySoon();
            }

            self._size = size;
            self.filename = target;
            self._stream = fs.createWriteStream(fullname, self.options);
            self._stream.on('error', function(error){
                if (self._failures < self.maxRetries) {
                    self._createStream();
                    self._failures++;
                }
                else {
                    self.emit('error', error);
                }
            });

            //
            // We need to listen for drain events when
            // write() returns false. This can make node
            // mad at times.
            //
            self._stream.setMaxListeners(Infinity);

            //
            // When the current stream has finished flushing
            // then we can be sure we have finished opening
            // and thus can emit the `open` event.
            //
            self.once('flush', function () {
                // Because "flush" event is based on native stream "drain" event,
                // logs could be written inbetween "self.flush()" and here
                // Therefore, we need to flush again to make sure everything is flushed
                self.flush();

                self.opening = false;
                self.emit('open', fullname);
            });

            //
            // Remark: It is possible that in the time it has taken to find the
            // next logfile to be written more data than `maxsize` has been buffered,
            // but for sensible limits (10s - 100s of MB) this seems unlikely in less
            // than one second.
            //
            self.flush();
        }

        fs.stat(fullname, function (err, stats) {
            if (err) {
                if (err.code !== 'ENOENT') {
                    return self.emit('error', err);
                }

                return createAndFlush(0);
            }

            if (!stats || (self.maxsize && stats.size >= self.maxsize)) {
                //
                // If `stats.size` is greater than the `maxsize` for
                // this instance then try again
                //
                return self._incFile(function() {
                    checkFile(self._getFile());
                }, true);
            }

            var now = new Date();
            if (self._year < now.getFullYear() || self._month < now.getMonth() || self._date < now.getDate() || self._hour < now.getHours()) {
                self._year   = now.getFullYear();
                self._month  = now.getMonth();
                self._date   = now.getDate();
                self._hour   = now.getHours();
                self._minute = now.getMinutes();
                self._created  = 0;
                return self._incFile(function() {
                    checkFile(self._getFile());
                });
            }

            createAndFlush(stats.size);
        });
    })(this._getFile());
};

HourlyRotateFile.prototype._incFile = function (callback, inc) {
    var ext = path.extname(this._basename);
    var basename = path.basename(this._basename, ext);

    if (!this.tailable) {
        if (inc) {
            this._created += 1;
        }
        this._checkMaxFilesIncrementing(ext, basename, callback);
    }
    else {
        this._checkMaxFilesTailable(ext, basename, this.getFormattedDate(), callback);
    }
};

//
// ### @private function _getFile ()
// Gets the next filename to use for this instance
// in the case that log filesizes are being capped.
//
HourlyRotateFile.prototype._getFile = function () {
    var ext = path.extname(this._basename),
        basename = path.basename(this._basename, ext);

    var filename = basename + this.getFormattedDate() + ext;

    if (this.tailable) {
        return basename + ext + ("string" === typeof this.tailable ? this.tailable : "");
    }

    return !this.tailable && this._created ?
        filename + '.' + this._created :
        filename;
};

//
// ### @private function _checkMaxFilesIncrementing ()
// Increment the number of files created or
// checked by this instance.
//
HourlyRotateFile.prototype._checkMaxFilesIncrementing = function (ext, basename, callback) {
    var oldest, target,
        self = this;

    // Check for maxFiles option and delete file
    if (!self.maxFiles || self._created < self.maxFiles) {
        return callback();
    }

    oldest = self._created - self.maxFiles;
    target = path.join(self.dirname, basename + (oldest !== 0 ? oldest : '') + ext);
    fs.unlink(target, callback);
};

//
// ### @private function _checkMaxFilesTailable ()
//
// Roll files forward based on integer, up to maxFiles.
// e.g. if base if file.log and it becomes oversized, roll
//    to file1.log, and allow file.log to be re-used. If
//    file is oversized again, roll file1.log to file2.log,
//    roll file.log to file1.log, and so on.
HourlyRotateFile.prototype._checkMaxFilesTailable = function (ext, basename, time, callback) {
        var self = this;

    fs.rename(
        path.join(self.dirname, basename + ext),
        path.join(self.dirname, basename + time + ext),
        callback
    );
};
